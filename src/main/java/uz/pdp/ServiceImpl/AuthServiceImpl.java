package uz.pdp.ServiceImpl;

import com.google.gson.Gson;
import uz.pdp.Service.AuthService;
import uz.pdp.Service.Util;
import uz.pdp.abs.User;
import uz.pdp.model.Customer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import static uz.pdp.DataBase.userList;
import static uz.pdp.Service.SessionMessage.*;
import static uz.pdp.Service.Util.*;

public class AuthServiceImpl implements AuthService {
    static AdminServiceImpl adminService = new AdminServiceImpl();
    static MainServiceImpl mainService = new MainServiceImpl();

    @Override
    public void login() {
        print(CYAN, "Enter username");
        String username = Util.inputStr();

        print(CYAN, "Enter password");
        String password = Util.inputStr();
        for (User user : userList) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)){
                switch (user.getRole()) {
                    case ADMIN:
                        adminService.adminMenu();
                        break;
                    case CUSTOMER:
                        mainService.showMenu((Customer) user);
                        break;
                }
            }
        }
    }

    @Override
    public void register() {
        print(CYAN, "Enter fullName");
        String fullName = Util.inputStr();
        print(CYAN, "Enter username");
        String username = Util.inputStr();
        print(CYAN, "Enter password");
        String password = Util.inputStr();
        print(CYAN, "Enter confirmPassword");
        String confirmPassword = Util.inputStr();
        if (password.equals(confirmPassword)) {
            Customer customer = new Customer(fullName, username, password, 0);
            userList.add(customer);
            writeJson();
            print(CYAN, CREATED);
        } else {
            print(RED, ERROR);
        }
    }

    public void writeJson() {
        try (Writer writer = new FileWriter("src/main/resources/user/user.json")) {
            Gson gson = new Gson();
            String s = gson.toJson(userList);
            writer.write(s);
            print(BLUE, WRITED);
        } catch (IOException e) {
            print(RED, NOT_FOUND);
        }
    }

    public void showUsers() {
        userList.forEach(user -> print(GREEN, "NAME : " + user.getFullName() + ", ROLE : " + user.getRole()));
    }
}
