package uz.pdp.abs;

import uz.pdp.model.Cloth;

public abstract class AbsClothQuantity {
    private Cloth cloth;
    private int quantity = 5;

    public AbsClothQuantity() {
    }

    public AbsClothQuantity(Cloth cloth, int quantity) {
        this.cloth = cloth;
        this.quantity = quantity;
    }

    public Cloth getCloth() {
        return cloth;
    }

    public void setCloth(Cloth cloth) {
        this.cloth = cloth;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "AbsClothQuantity{" +
                "cloth=" + cloth +
                ", quantity=" + quantity +
                '}';
    }
}
