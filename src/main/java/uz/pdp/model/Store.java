package uz.pdp.model;

import uz.pdp.abs.AbsClothQuantity;

public class Store extends AbsClothQuantity {
    public Store() {
    }

    public Store(Cloth cloth, int quantity) {
        super(cloth, quantity);
    }
}
