package uz.pdp.Service;

import uz.pdp.abs.User;

public interface OrderService {
    
    void addToCart(User user);

    void orderMenu(User user);

    void myCart(User user);
}

