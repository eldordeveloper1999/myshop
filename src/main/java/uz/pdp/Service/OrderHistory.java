package uz.pdp.Service;

import uz.pdp.abs.User;

public interface OrderHistory {
    void orderHistory(User user);

    void orderHistoryPayType(User user);

    void convertExcel(User user);

    void orderHistoryMenu(User user);
}
